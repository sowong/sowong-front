/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

exports.onCreatePage = async ({page, actions}) => {
    if (page.path.match(/^\/product/)) {
        page.matchPath = '/product/*'
        actions.createPage(page)
    }
}