# Product Hihi
It is a project aiming to chart the background of every product in Hong Kong people's life.

_Disclaimer: This thing is in alpha stage. Don't expect it to just include everything._

## Development
Generated from redux starter by [caki0915](https://github.com/caki0915/gatsby-starter-redux). See [README-redux-starter.md](./README-redux-starter.md) for the original README.

## Drafts
So don't expect these to be organized. It will become more so over time.

### App story
Long story short:
1. search by cat (by clicking + suggesting categories **first** when user searches with text)
2. barcode search / narrow search: name + brand
#### Use case 1: looking for some more-or-less broad category
1. User enters app
2. App shows a search box. Popular tag shown for convenience
3. User enters search terms in search box / select on a popular tag
4. Results shows: <!--(in order of appearance / relevance)-->
   1. Products
      1. Image
      2. Name
      3. Category
      4. China-ness

   Should we also display categories / origin / other tags? let's suggest tags based on current search term maybe
5. User clicks on product
   1. Display:
      1. Name
      2. Overall China-ness
      3. Image
      4. Barcode
      5. Origin
      6. Brand
         1. Closeness to China by scoring system

         _Caution! Do not display details here like brand major shareholder, shares, China market etc._

      7. Distributors
         1. One or a few distributors this product is imported by
         2. How close each distributors is to China

         _Same caution as above_
      8. Related products at the bottom
         1. Goal: Display **closest** alternatives
         2. Displayed as an horizontal carousel, sorted China-ness in dsc. order

#### Use case 2: look for an alternative of existing products
1. User enters app
2. User enters barcode of the product
3. User clicks on the product
4. User looks for relevant products at the bottom of product details

### How to search
1. By category, by name, by tags, by barcode
2. By text eventually though
3. Category, tags suggested as visual clickable tags. A click prepend it to the search terms
   1. Like how google does with images search
   2. Like this:

    ![image search term "carrie lam" on google with "li keqiang" tag selected on desktop PC](777+chi-na.png)

    or mobile:

    ![image search term "carrie lam" on google with "li keqiang" tag selected on mobile phone](777+chi-na-mobile.jpg)

### Pages
Search oriented
1. By category
2. By name
3. By tags
4. By barcode
#### Home
Search box at top;

(Popular) Categories next;

#### Search Result layout
##### Mobile
TileList
##### Desktop
TileGrid
##### Content
Each tile is a product. Shows:
1. Name
2. Brand
3. China-ness
4. Origin
5. Image

#### Category page
Shares search result layout. With a search box to refine search may be?

### Data to be shown to users (by entities)
#### Product
1. Barcode
2. Name
3. Price (approx. only. Only accurate at the time of data acquisition.)
4. Origin
5. Manufaturer
6. Brand (should be mostly accurate. Expect to be updated through feedbacks)
7. Distributor (again not accurate. Only as accurate as the brand's. A brand can have multiple distributors. See "[Brand](#brand)").
8. Image (might not present in data acquired)

#### Brand
1. Name
2. Owner (company)
3. Distributors

#### Company (distributors, brand owners)
1. Name
2. Major shareholders info (affilation with CCP? or any other political powers)
3. Origin of Material imported (obviously brand owners only)
4. Mainland market weight in company business
5. Headquartered at (which legal economic zone)
6. Affiliation with political powers

### Data transformations to be done (goals only. Not prioritized yet.)
#### Evaluate affiliations and display summary sensibly to users
e.g. Company ABC has 70% of share controlled by 人大常委(s) -> Strongly affiliated with China

e.g. Company ABC has its production process done in China


##### Ways to represent these so far not very concrete "signs" to look for in a product
A thought: Use icons to represent these "signs"

Neesan:
- Signs個度係咪會有人大、黃色經濟同藍到黑三個 signs
#### Scoring system
Based on the brand's affiliation with political powers, origin of material imported, shareholder info, etc., create a score to easily distinguish products that would benefit Hong Kong people if supported.

Some rules right now:
1. Minimize revenue flowing to China
2. Support local brands
3. Support brands friendly with Hong Kong, either ideologically, actually or geopolitically 
