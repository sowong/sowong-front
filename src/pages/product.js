import React from 'react';
import { Link } from 'gatsby';
import { Router } from '@reach/router'
import Layout from '../components/layout';
import Image from '../components/image';
import SEO from '../components/seo';
import { TextField, Container, Grid, styled, Box, Chip, withStyles, createStyles, Typography, Card, CardMedia, CardContent, Tooltip, SvgIcon } from '@material-ui/core';
import GatsbyImage from 'gatsby-image';
import { TransitionGroup } from 'react-transition-group'
import { product1 } from '../dummies/products';
import ProductDetails from '../components/ProductDetails';

const ProductDetailPageTemplate = ({id, product }) => (
  <>
    <SEO title="Meow" keywords={[`喵`, `にゃー`, `miaow`]} />
    <Container>
      <ProductDetails product={product1} />
    </Container>
  </>
)

const ProductDetailPage = ({classes, product}) => (
  <Layout>
    <Router>
      <ProductDetailPageTemplate path='/product/:id' />
    </Router>
  </Layout>
);


export default ProductDetailPage;
