import React from 'react';
import { Link } from 'gatsby';

import Layout from '../components/layout';
import Image from '../components/image';
import SEO from '../components/seo';
import { TextField, Container, Grid, styled, Box, Chip, withStyles, createStyles, Typography, Card, CardMedia, CardContent, Tooltip, SvgIcon } from '@material-ui/core';
import GatsbyImage from 'gatsby-image';
import { TransitionGroup } from 'react-transition-group'
import SearchInputs from '../components/SearchInputs';
import SearchResults from '../components/SearchResults';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <Container>
      <SearchInputs />
      <SearchResults />
    </Container>
  </Layout>
);


export default IndexPage;
