import React from 'react';
import { TextField, Container, Grid, styled, Box, Chip, withStyles, createStyles, Typography, Card, CardMedia, CardContent, Tooltip, SvgIcon } from '@material-ui/core';
import LazyLoad from 'react-lazyload';
import {ThumbUpOutlined as ThumbUp, EmojiPeople} from '@material-ui/icons'
import {FaHardHat} from 'react-icons/fa'
import indicatorMeta from '../dummies/indicators'
import { product1, product2 } from '../dummies/products';
import ProductItem from '../components/ProductItem'

const gen = function* (start, fin) {
  for (let i = start; i < fin; i++) {
    yield i
  }
}

const styles = theme => createStyles({
  root: {
    position: 'relative',
  },
  pending: {
    '&::after': {
      content: '"Loading..."',
      background: 'rgba(255, 255, 255, 0.3)',
      zIndex: 2,
      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      padding: theme.spacing(5),
      textAlign: 'center'
    },
  }
})

const Results = ({classes, products, pending}) => (
  <Grid container spacing={2} className={`${classes.root} ${pending ? classes.pending : ''}`}>
      {products.map(p => <Grid key={p.id} item xs={12} sm={6} md={3}>
          <ProductItem  product={p}/>
      </Grid>)}
  </Grid>
)

const products = [
    ...Array.from(gen(0, 10)).map(n => ({...product1, id: n})),
    ...Array.from(gen(10, 20)).map(n => ({...product2, id: n})),
]

export default withStyles(styles)((props) => <Results products={products} {...props}/>)