import React from 'react';
import Link from 'gatsby-link';
import styled from '@emotion/styled';
import { connect } from 'react-redux';
import { Container, withStyles, createStyles } from '@material-ui/core';

const Navbar = withStyles(theme => createStyles({
  root: {
    height: `${theme.spacing(4)}px`,
    width: '100vw',
    display: 'flex',
    alignItems: 'center',
    background: theme.palette.primary.main,
  }
}))(({ classes: { root: className }, ...otherProps }) => <nav className={className} {...otherProps} />)

const Title = withStyles(theme => createStyles({
  root: {
    color: theme.palette.primary.contrastText,
    marginBottom: 0,
    whiteSpace: 'nowrap',
  }
}))(({ classes: { root: className }, ...otherProps }) => <h2 className={className} {...otherProps} />)

const Header = ({ siteTitle }) => (
  <Navbar>
    <Container>
      <Link to="/" css={{ textDecoration: 'none' }}>
        <Title>{siteTitle}</Title>
      </Link>
    </Container>
  </Navbar>
);

export default Header
