import React from 'react';
import { TextField, Container, Grid, styled, Box, Chip, withStyles, createStyles, Typography, Card, CardMedia, CardContent, Tooltip, SvgIcon, withTheme } from '@material-ui/core';
import { connect } from 'react-redux';
import { setSearchTerm, selectTag } from '../state/search';

const TagRow = ({tags = [], selectedTag, selectTag}) => <div>
  {tags.map(t => <SearchTag tag={t} key={t} selected={selectedTag === t} 
    selectTag={selectTag}
  />)}
</div>

const SearchTag = withStyles(theme => createStyles({
  root: {
    cursor: 'pointer',
    '&:hover': {
      background: theme.palette.grey[400]
    },
  },
}))(withTheme(({tag, classes: {root}, selected, selectTag, theme}) => (
  <Chip label={tag} className={root} 
    onClick={() => {
      if (!selected) {selectTag(tag)}
      else {selectTag(null)}
    }}
  style={selected ? { background: theme.palette.grey[500] } : {}}
  />
)))

const mapState = (state, ownProps) => {
  const {term, selectedTag} = state.search
  return {
    term,
    selectedTag,
  }
}

const mapDispatch = (dispatch) => {
  return {
    selectTag: tag => dispatch(selectTag(tag)),
  }
}

export default connect(
  mapState,
  mapDispatch,
)(({term, selectedTag, selectTag,}) => (
  <Box display="flex" flexDirection="column">
    <Typography variant='caption'>Search</Typography>
    <TextField />
    <Typography variant='caption'>and / or click on categories</Typography>
    <TagRow tags={['油', '即食麵', '米']} selectedTag={selectedTag} selectTag={selectTag}/>
  </Box>
))

