import React from 'react'
import { TextField, Container, Grid, styled, Box, Chip, withStyles, createStyles, Typography, Card, CardMedia, CardContent, Tooltip, SvgIcon } from '@material-ui/core';
import indicators from '../dummies/indicators';
import ProductItem from './ProductItem';
import { product2 } from '../dummies/products';
import LazyLoad from 'react-lazyload';

const styles = theme => createStyles({
  img: {
    width: 320,
  },
})

const indicatorStyles = theme => createStyles({
  root: {
    display: 'flex',
    alignItems: 'center',
    '&>:first-child': {
      marginRight: theme.spacing(1),
      width: theme.spacing(3),
      maxWidth: theme.spacing(3),
    },
  }
})

const Indicator = withStyles(indicatorStyles)(({ indicator, classes }) => (
  <div className={classes.root}>
    {React.createElement(React.forwardRef(indicators[indicator].component))}
    <Typography component='span'>{indicators[indicator].desc}</Typography>
  </div>
))

const ProductDetails = ({ classes, product: { name, category, brand, indicators, } }) => (
  <>
    <section>
      <Grid container>
        <Grid item xs={12} sm={4}>
          <LazyLoad>
            <img src='https://www.parknshop.com/medias/sys_master/front/zoom/8897016135710.jpg' />
          </LazyLoad>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Typography variant='overline'>{brand}</Typography>
          <Typography variant='h6'>{name}</Typography>
          <Typography variant='caption'>{category}</Typography>
          <div>
            <Typography variant='caption'>重要指標</Typography>
            {indicators.map(i => <Indicator indicator={i} key={i}/>)}
          </div>
          <div>
            <Typography variant='caption'>經銷商</Typography>
            <Typography>百佳</Typography>
          </div>
          <div>
            <Typography variant='caption'>Barcode</Typography>
            <Typography>12345678910</Typography>
          </div>
        </Grid>
      </Grid>
    </section>
    <section>
      <Typography variant='caption'>推薦產品</Typography>
      <Grid container>
        <Grid item xs={3}>
          <ProductItem product={product2} />
        </Grid>
        <Grid item xs={3}>
          <ProductItem product={product2} />
        </Grid>
        <Grid item xs={3}>
          <ProductItem product={product2} />
        </Grid>
      </Grid>
    </section>
  </>
)

export default withStyles(styles)(ProductDetails)