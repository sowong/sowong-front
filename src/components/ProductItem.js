import React from 'react';
import { TextField, Container, Grid, styled, Box, Chip, withStyles, createStyles, Typography, Card, CardMedia, CardContent, Tooltip, SvgIcon, Link } from '@material-ui/core';
import LazyLoad from 'react-lazyload';
import {ThumbUpOutlined as ThumbUp, EmojiPeople} from '@material-ui/icons'
import {FaHardHat} from 'react-icons/fa'

const imgHeight = 90

const Img = withStyles(theme => createStyles({
  media: {
    height: imgHeight,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundColor: '#f4f4f4',
  },
}))(({classes, imgUrl, ...otherProps}) => (
  <div className={classes.media}
    style={{backgroundImage: `url("${imgUrl}")`}}
  />
))

const productItemStyles = theme => createStyles({
  card: {
    width: 160,
  },
  content: {
    flexGrow: 1,
    flexShrink: 0,
    padding: theme.spacing(2),
    overflow: 'hidden',
    flexBasis: 150,
  },
  contentTitle: {
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
  },
})

const ProductLink = ({product: {id}, children, ...others},) => (
  <Link href={`/product/${id}`} {...others}>
    {children}
  </Link>
)

export default withStyles(productItemStyles)(({ classes, product, product: { img, name, category, indicators }, }) => (
  <Card className={classes.card}>
    <LazyLoad>
      <ProductLink product={product}>
        <Img imgUrl={img} />
      </ProductLink>
    </LazyLoad>
    <Grid container>
      <Grid item className={classes.content}>
        <ProductLink product={product}>
          <Typography variant='body2' component='h6' className={classes.contentTitle}>{name}</Typography>
        </ProductLink>
        <Typography variant='caption'>{category}</Typography>
      </Grid>
    </Grid>
  </Card>
))
