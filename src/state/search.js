const initialState = {
  term: '',
  selectedTag: null,
};

const SET_SEARCH_TERM = 'SET_SEARCH_TERM'
const SELECT_TAG = 'SELECT_TAG'
export const selectTag = tag => ({ type: SELECT_TAG, payload: tag });
export const setSearchTerm = term => ({ type: SET_SEARCH_TERM, payload: term });

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SELECT_TAG:
      return {...state, selectedTag: payload}
    case SET_SEARCH_TERM:
      return {...state, term: payload}
    default:
      return state;
  }
};
