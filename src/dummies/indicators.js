import React from 'react'
import {Typography, SvgIcon, withStyles, createStyles} from '@material-ui/core'
import IndicatorHK from '../images/indicator-hk.svg'
import IndicatorShing from '../images/indicator-shing.svg'
import IndicatorBlue from '../images/indicator-blue.svg'
import IndicatorYellow from '../images/indicator-yellow.svg'

const SvgIndicator = withStyles(theme => createStyles({
  root: {
    minWidth: theme.spacing(3),
    maxWidth: theme.spacing(3),
  },
}))(({indicator: Indicator, classes}) => <SvgIcon className={classes.root}><Indicator /></SvgIcon>)

export default {
  'HK': {
    component: (props, ref) => <SvgIndicator indicator={IndicatorHK} />,
    desc: '香港品牌',
  },
  '誠': {
    component: (props, ref) => <SvgIndicator indicator={IndicatorShing} />,
    desc: '誠哥',
  },
  'blue': {
    component: (props, ref) => <SvgIndicator indicator={IndicatorBlue} />,
    desc: '藍',
  },
  'yellow': {
    component: (props, ref) => <SvgIndicator indicator={IndicatorYellow} />,
    desc: '黃',
  }
}
