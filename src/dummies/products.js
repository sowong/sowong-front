export const product1 = {
  name: '混合米—印度香糙米—紅米—野米',
  category: '米',
  img: 'https://www.parknshop.com/medias/sys_master/front/zoom/8897016135710.jpg',
  id: 'uuid',
  amount: '1KG',
  origin: '英國',
  barcode: '4898118951965',
  indicators: ['HK', '誠', 'blue', 'yellow',],
  brand: '維狄',
}

export const product2 = {
  name: '開心落原餐',
  category: '即食麵',
  img: 'https://www.google.com/logos/doodles/2019/new-years-eve-2019-4659144240922624-l.png',
  indicators: ['HK', 'blue', '誠'],
  id: 'uuid',
  amount: '1KG',
  origin: '英國',
  brand: '金拱門',
}